# Cleeng API

## Uruchomienie

- docker-compose build
- docker-compose up -d
- docker exec -it api composer install
- sudo chgrp -R www-data storage bootstrap/cache
- sudo chmod -R ug+rwx storage bootstrap/cache
- cp .env.example .env
- docker exec -it api php artisan key:generate
- docker exec -it api php artisan migrate:refresh --seed
- do pliku hosts należy dodać adres 127.0.0.1 api.local

## Użycie

### Dodanie video
#### POST api.local/api/v1/video

- name:string
- link:string
- event:bool

### Przypisanie video do kategorii
#### POST api.local/api/v1/video/{videoId}/category/{categoryId}

### Przypisanie kategorii do subskrypcji
#### POST api.local/api/v1/category/{categoryId}/subscription/{subscriptionId}

### Zakup dostępu do video
#### POST api.local/api/v1/payment/user/{userId}/video/{videoId}

### Zakup subskrypcji
#### POST api.local/api/v1/payment/user/{userId}/subscription/{subscriptionId}

### Pobranie video
#### GET api.local/api/v1/stream/video/{videoId}/user/{userId}