<?php
declare(strict_types=1);

namespace Cleeng\Category;

interface CategoryRepository
{
    /**
     * @throws CategoryNotFoundException
     */
    function find(int $categoryId): Category;
}