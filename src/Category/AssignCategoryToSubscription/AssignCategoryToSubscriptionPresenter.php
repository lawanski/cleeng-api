<?php
declare(strict_types=1);

namespace Cleeng\Category\AssignCategoryToSubscription;

interface AssignCategoryToSubscriptionPresenter
{
    function present(AssignCategoryToSubscriptionResponse $response): void;
    function view();
}