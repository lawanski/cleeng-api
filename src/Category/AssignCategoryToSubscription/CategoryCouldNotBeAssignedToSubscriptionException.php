<?php
declare(strict_types=1);

namespace Cleeng\Category\AssignCategoryToSubscription;

use Cleeng\Category\CategoryException;

class CategoryCouldNotBeAssignedToSubscriptionException extends CategoryException
{
    protected $message = 'Category could not be assigned to subscription';
    protected $code = 422;
}