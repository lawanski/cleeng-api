<?php
declare(strict_types=1);

namespace Cleeng\Category\AssignCategoryToSubscription;

class AssignCategoryToSubscriptionResponse
{
    private $exception;

    function exceptionOccurred(CategoryCouldNotBeAssignedToSubscriptionException $exception)
    {
        $this->exception = $exception;
    }

    function isCategorySuccessfullyAssignedToSubscription(): bool
    {
        if ($this->exception instanceof CategoryCouldNotBeAssignedToSubscriptionException) {
            throw $this->exception;
        }

        return true;
    }
}