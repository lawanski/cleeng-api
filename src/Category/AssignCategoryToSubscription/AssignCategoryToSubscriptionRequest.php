<?php
declare(strict_types=1);

namespace Cleeng\Category\AssignCategoryToSubscription;

class AssignCategoryToSubscriptionRequest
{
    private $categoryId;
    private $subscriptionId;

    function __construct($categoryId, $subscriptionId)
    {
        $this->categoryId = $categoryId;
        $this->subscriptionId = $subscriptionId;
    }

    public function getCategoryId(): int
    {
        return (int)$this->categoryId;
    }

    public function getSubscriptionId(): int
    {
        return (int)$this->subscriptionId;
    }
}