<?php
declare(strict_types=1);

namespace Cleeng\Category\AssignCategoryToSubscription;

interface AssignCategoryToSubscriptionQueryObject
{
    /**
     * @throws CategoryCouldNotBeAssignedToSubscriptionException
     */
    function execute(int $categoryId, int $subscriptionId): void;
}