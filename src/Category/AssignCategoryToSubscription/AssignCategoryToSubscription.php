<?php
declare(strict_types=1);

namespace Cleeng\Category\AssignCategoryToSubscription;

class AssignCategoryToSubscription
{
    private $assignCategoryToSubscriptionQueryObject;

    function __construct(AssignCategoryToSubscriptionQueryObject $assignCategoryToSubscriptionQueryObject)
    {
        $this->assignCategoryToSubscriptionQueryObject = $assignCategoryToSubscriptionQueryObject;
    }

    function execute(AssignCategoryToSubscriptionRequest $request, AssignCategoryToSubscriptionPresenter $presenter)
    {
        $response = new AssignCategoryToSubscriptionResponse();

        try {
            $this->assignCategoryToSubscriptionQueryObject->execute(
                $request->getCategoryId(),
                $request->getSubscriptionId()
            );
        } catch (CategoryCouldNotBeAssignedToSubscriptionException $exception) {
            $response->exceptionOccurred($exception);
        }

        $presenter->present($response);
    }
}