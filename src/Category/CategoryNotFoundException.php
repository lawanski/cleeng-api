<?php
declare(strict_types=1);

namespace Cleeng\Category;

class CategoryNotFoundException extends CategoryException
{
    protected $message = 'Category not found';
    protected $code = 404;
}