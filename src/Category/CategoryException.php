<?php
declare(strict_types=1);

namespace Cleeng\Category;

class CategoryException extends \Exception
{}