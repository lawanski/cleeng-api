<?php
declare(strict_types=1);

namespace Cleeng\Subscription\BuySubscription;

use Cleeng\Subscription\SubscriptionException;

class UserDidNotBoughtSubscriptionException extends SubscriptionException
{
    protected $message = 'User did not bought subscription';
}