<?php
declare(strict_types=1);

namespace Cleeng\Subscription\BuySubscription;

class BuySubscriptionResponse
{
    private $exception;

    function exceptionOccurred(UserDidNotBoughtSubscriptionException $exception): void
    {
        $this->exception = $exception;
    }

    /**
     * @throws UserDidNotBoughtSubscriptionException
     */
    function didUserBoughtSubscription(): bool
    {
        if ($this->exception instanceof UserDidNotBoughtSubscriptionException) {
            throw $this->exception;
        }

        return true;
    }
}