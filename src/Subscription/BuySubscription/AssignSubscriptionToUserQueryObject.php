<?php
declare(strict_types=1);

namespace Cleeng\Subscription\BuySubscription;

use Cleeng\Payment\Payment;
use Cleeng\Subscription\Subscription;
use Cleeng\User\User;

interface AssignSubscriptionToUserQueryObject
{
    /**
     * @throws SubscriptionCouldNotBeAssignedToUserException
     */
    function execute(Subscription $subscription, User $user, Payment $payment): void;
}