<?php
declare(strict_types=1);

namespace Cleeng\Subscription\BuySubscription;

interface BuySubscriptionPresenter
{
    function present(BuySubscriptionResponse $response): void;
    function view();
}