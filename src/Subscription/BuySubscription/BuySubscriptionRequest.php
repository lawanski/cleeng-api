<?php
declare(strict_types=1);

namespace Cleeng\Subscription\BuySubscription;

class BuySubscriptionRequest
{
    private $userId;
    private $subscriptionId;

    function __construct(
        $userId,
        $subscriptionId
    ) {
        $this->userId = $userId;
        $this->subscriptionId = $subscriptionId;
    }

    public function getUserId(): int
    {
        return (int)$this->userId;
    }

    public function getSubscriptionId(): int
    {
        return (int)$this->subscriptionId;
    }
}