<?php
declare(strict_types=1);

namespace Cleeng\Subscription\BuySubscription;

use Cleeng\Payment\Payment;
use Cleeng\Payment\PaymentNotInsertedException;
use Cleeng\Payment\PaymentRepository;
use Cleeng\Subscription\SubscriptionNotFoundException;
use Cleeng\Subscription\SubscriptionRepository;
use Cleeng\User\UserNotFoundException;
use Cleeng\User\UserRepository;

class BuySubscription
{
    private $userRepository;
    private $subscriptionRepository;
    private $paymentRepository;
    private $assignSubscriptionToUserQueryObject;

    public function __construct(
        UserRepository $userRepository,
        SubscriptionRepository $subscriptionRepository,
        PaymentRepository $paymentRepository,
        AssignSubscriptionToUserQueryObject $assignSubscriptionToUserQueryObject
    ) {
        $this->userRepository = $userRepository;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->paymentRepository = $paymentRepository;
        $this->assignSubscriptionToUserQueryObject = $assignSubscriptionToUserQueryObject;
    }

    function execute(BuySubscriptionRequest $request, BuySubscriptionPresenter $presenter)
    {
        $response = new BuySubscriptionResponse();

        try {
            $user = $this->userRepository->find($request->getUserId());
            $subscription = $this->subscriptionRepository->find($request->getSubscriptionId());

            $payment = new Payment();
            $payment->setCreatedAt(new \DateTime());
            $payment->setUserId($user->getId());

            $this->paymentRepository->insert($payment);

            $this->assignSubscriptionToUserQueryObject->execute($subscription, $user, $payment);
        } catch (
            UserNotFoundException|
            SubscriptionNotFoundException|
            PaymentNotInsertedException|
            SubscriptionCouldNotBeAssignedToUserException $exception
        ) {
            $response->exceptionOccurred(new UserDidNotBoughtSubscriptionException($exception->getMessage(), $exception->getCode()));
        }

        $presenter->present($response);
    }
}