<?php
declare(strict_types=1);

namespace Cleeng\Subscription\BuySubscription;

use Cleeng\Subscription\SubscriptionException;

class SubscriptionCouldNotBeAssignedToUserException extends SubscriptionException
{
    protected $message = 'Subscription could not be assigned to User';
    protected $code = 422;
}