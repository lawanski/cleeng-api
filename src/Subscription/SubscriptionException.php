<?php
declare(strict_types=1);

namespace Cleeng\Subscription;

class SubscriptionException extends \Exception
{}