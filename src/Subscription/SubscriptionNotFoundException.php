<?php
declare(strict_types=1);

namespace Cleeng\Subscription;

class SubscriptionNotFoundException extends SubscriptionException
{
    protected $message = 'Subscription not found';
    protected $code = 404;
}