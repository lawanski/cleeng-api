<?php
declare(strict_types=1);

namespace Cleeng\Subscription;

interface SubscriptionRepository
{
    /**
     * @throws SubscriptionNotFoundException
     */
    function find(int $subscriptionId): Subscription;
}