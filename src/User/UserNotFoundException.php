<?php
declare(strict_types=1);

namespace Cleeng\User;

class UserNotFoundException extends UserException
{
    protected $message = 'User not found';
    protected $code = 404;
}