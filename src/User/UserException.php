<?php
declare(strict_types=1);

namespace Cleeng\User;

class UserException extends \Exception
{}