<?php
declare(strict_types=1);

namespace Cleeng\User;

interface UserRepository
{
    /**
     * @throws UserNotFoundException
     */
    function find(int $userId): User;
}