<?php
declare(strict_types=1);

namespace Cleeng\Payment;

interface PaymentRepository
{
    /**
     * @throws PaymentNotInsertedException
     */
    function insert(Payment $payment): void;
}