<?php
declare(strict_types=1);

namespace Cleeng\Payment;

class PaymentNotInsertedException extends PaymentException
{
    protected $message = 'Payment not inserted';
    protected $code = 422;
}