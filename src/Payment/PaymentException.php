<?php
declare(strict_types=1);

namespace Cleeng\Payment;

class PaymentException extends \Exception
{}