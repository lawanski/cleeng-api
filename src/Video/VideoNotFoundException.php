<?php
declare(strict_types=1);

namespace Cleeng\Video;

class VideoNotFoundException extends VideoException
{
    protected $message = 'Video not found';
    protected $code = 404;
}