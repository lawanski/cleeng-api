<?php
declare(strict_types=1);

namespace Cleeng\Video\BuyVideo;

class BuyVideoResponse
{
    private $exception;

    function exceptionOccurred(UserDidNotBoughtVideoException $exception)
    {
        $this->exception = $exception;
    }

    function isVideoBoughtSuccessfully(): bool
    {
        if ($this->exception instanceof UserDidNotBoughtVideoException) {
            throw $this->exception;
        }

        return true;
    }
}