<?php
declare(strict_types=1);

namespace Cleeng\Video\BuyVideo;

interface BuyVideoPresenter
{
    function present(BuyVideoResponse $response): void;
    function view();
}