<?php
declare(strict_types=1);

namespace Cleeng\Video\BuyVideo;

use Cleeng\Video\VideoException;

class VideoCouldNotBeAssignedToUserException extends VideoException
{
    protected $message = 'Video could not be assigned to user';
    protected $code = 422;
}