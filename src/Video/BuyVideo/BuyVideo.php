<?php
declare(strict_types=1);

namespace Cleeng\Video\BuyVideo;

use Cleeng\Payment\Payment;
use Cleeng\Payment\PaymentNotInsertedException;
use Cleeng\Payment\PaymentRepository;
use Cleeng\User\UserNotFoundException;
use Cleeng\User\UserRepository;
use Cleeng\Video\VideoNotFoundException;
use Cleeng\Video\VideoRepository;

class BuyVideo
{
    private $userRepository;
    private $videoRepository;
    private $paymentRepository;
    private $assignVideoToUserQueryObject;

    function __construct(
        UserRepository $userRepository,
        VideoRepository $videoRepository,
        PaymentRepository $paymentRepository,
        AssignVideoToUserQueryObject $assignVideoToUserQueryObject
    ) {
        $this->userRepository = $userRepository;
        $this->videoRepository = $videoRepository;
        $this->paymentRepository = $paymentRepository;
        $this->assignVideoToUserQueryObject = $assignVideoToUserQueryObject;
    }

    function execute(BuyVideoRequest $request, BuyVideoPresenter $presenter)
    {
        $response = new BuyVideoResponse();

        try {
            $user = $this->userRepository->find($request->getUserId());
            $video = $this->videoRepository->find($request->getVideoId());

            $payment = new Payment();
            $payment->setUserId($user->getId());
            $payment->setCreatedAt(new \DateTime());

            $this->paymentRepository->insert($payment);

            $this->assignVideoToUserQueryObject->execute($user, $video, $payment);
        } catch (
            UserNotFoundException|
            VideoNotFoundException|
            PaymentNotInsertedException|
            VideoCouldNotBeAssignedToUserException $exception
        ) {
            $response->exceptionOccurred(new UserDidNotBoughtVideoException(
                $exception->getMessage(),
                $exception->getCode())
            );
        }

        $presenter->present($response);
    }
}