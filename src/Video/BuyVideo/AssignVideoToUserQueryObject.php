<?php
declare(strict_types=1);

namespace Cleeng\Video\BuyVideo;

use Cleeng\Payment\Payment;
use Cleeng\User\User;
use Cleeng\Video\Video;

interface AssignVideoToUserQueryObject
{
    /**
     * @throws VideoCouldNotBeAssignedToUserException
     */
    function execute(User $user, Video $video, Payment $payment): void;
}