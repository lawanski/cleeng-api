<?php
declare(strict_types=1);

namespace Cleeng\Video\BuyVideo;

use Cleeng\Video\VideoException;

class UserDidNotBoughtVideoException extends VideoException
{
    protected $message = 'User did not bought subscription';
    protected $code = 422;
}