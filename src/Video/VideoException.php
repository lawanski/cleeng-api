<?php
declare(strict_types=1);

namespace Cleeng\Video;

class VideoException extends \Exception
{}