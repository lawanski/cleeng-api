<?php
declare(strict_types=1);

namespace Cleeng\Video\WatchVideo;

class WatchVideoRequest
{
    private $userId;
    private $videoId;

    function __construct(
        $userId,
        $videoId
    )
    {
        $this->userId = $userId;
        $this->videoId = $videoId;
    }

    public function getUserId(): int
    {
        return (int)$this->userId;
    }

    public function getVideoId(): string
    {
        return (string)$this->videoId;
    }
}