<?php
declare(strict_types=1);

namespace Cleeng\Video\WatchVideo;

interface WatchVideoPresenter
{
    function present(WatchVideoResponse $response): void;
    function view();
}