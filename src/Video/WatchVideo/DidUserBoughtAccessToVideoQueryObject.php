<?php
declare(strict_types=1);

namespace Cleeng\Video\WatchVideo;

use Cleeng\Video\Video;

interface DidUserBoughtAccessToVideoQueryObject
{
    /**
     * @throws UserDontHaveAccessToVideoException
     */
    function execute(int $userId, string $videoId): Video;
}