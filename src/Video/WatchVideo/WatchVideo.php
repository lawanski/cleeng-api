<?php
declare(strict_types=1);

namespace Cleeng\Video\WatchVideo;

class WatchVideo
{
    private $didUserBoughtAccessToVideoQueryObject;

    function __construct(DidUserBoughtAccessToVideoQueryObject $didUserBoughtAccessToVideoQueryObject)
    {
        $this->didUserBoughtAccessToVideoQueryObject = $didUserBoughtAccessToVideoQueryObject;
    }

    function execute(WatchVideoRequest $request, WatchVideoPresenter $presenter)
    {
        $response = new WatchVideoResponse();

        try {
            $video = $this->didUserBoughtAccessToVideoQueryObject->execute($request->getUserId(), $request->getVideoId());
            $response->setVideo($video);
        } catch (UserDontHaveAccessToVideoException $exception) {
            $response->exceptionOccurred($exception);
        }

        $presenter->present($response);
    }
}