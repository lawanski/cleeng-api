<?php
declare(strict_types=1);

namespace Cleeng\Video\WatchVideo;

use Cleeng\Video\Video;

class WatchVideoResponse
{
    private $video;
    private $exception;

    /**
     * @throws UserDontHaveAccessToVideoException
     */
    function getVideo(): Video
    {
        if (!$this->video instanceof Video) {
            throw $this->exception;
        }

        return $this->video;
    }

    function setVideo(Video $video): void
    {
        $this->video = $video;
    }

    function exceptionOccurred(UserDontHaveAccessToVideoException $exception): void
    {
        $this->exception = $exception;
    }
}