<?php
declare(strict_types=1);

namespace Cleeng\Video\WatchVideo;

use Cleeng\Video\VideoException;

class UserDontHaveAccessToVideoException extends VideoException
{
    protected $message = "User don't have access to watch this video";
    protected $code = 403;
}