<?php
declare(strict_types=1);

namespace Cleeng\Video;

class VideoNotStoredException extends VideoException
{
    protected $message = 'Video not stored';
    protected $code = 422;
}