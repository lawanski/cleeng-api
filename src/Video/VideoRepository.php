<?php
declare(strict_types=1);

namespace Cleeng\Video;

interface VideoRepository
{
    /**
     * @throws VideoNotStoredException
     */
    function insert(Video $video): void;

    /**
     * @throws VideoNotFoundException
     */
    function find(string $videoId): Video;
}