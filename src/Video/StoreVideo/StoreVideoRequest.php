<?php
declare(strict_types=1);

namespace Cleeng\Video\StoreVideo;

class StoreVideoRequest
{
    private $name;
    private $link;
    private $event;

    public function __construct(
        $name,
        $link,
        $event
    )
    {
        $this->name = $name;
        $this->link = $link;
        $this->event = $event;
    }

    public function getName(): string
    {
        return (string)$this->name;
    }

    public function getLink(): string
    {
        return (string)$this->link;
    }

    public function getEvent(): bool
    {
        return (bool)$this->event;
    }
}