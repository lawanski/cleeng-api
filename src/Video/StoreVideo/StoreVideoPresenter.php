<?php
declare(strict_types=1);

namespace Cleeng\Video\StoreVideo;

interface StoreVideoPresenter
{
    function present(StoreVideoResponse $response): void;
    function view();
}