<?php
declare(strict_types=1);

namespace Cleeng\Video\StoreVideo;

use Cleeng\Video\Video;
use Cleeng\Video\VideoNotStoredException;
use Cleeng\Video\VideoRepository;

class StoreVideo
{
    private $repository;

    public function __construct(VideoRepository $repository)
    {
        $this->repository = $repository;
    }

    function execute(StoreVideoRequest $request, StoreVideoPresenter $presenter)
    {
        $response = new StoreVideoResponse();

        $video = new Video();
        $video->setId(uniqid());
        $video->setName($request->getName());
        $video->setLink($request->getLink());
        $video->setEvent($request->getEvent());

        try {
            $this->repository->insert($video);
            $response->setVideo($video);
        } catch (VideoNotStoredException $exception) {
            $response->exceptionOccurred($exception);
        }

        $presenter->present($response);
    }
}