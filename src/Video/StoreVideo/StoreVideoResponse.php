<?php
declare(strict_types=1);

namespace Cleeng\Video\StoreVideo;

use Cleeng\Video\Video;
use Cleeng\Video\VideoNotStoredException;

class StoreVideoResponse
{
    private $video;
    private $exception;

    function setVideo(Video $video): void
    {
        $this->video = $video;
    }

    /**
     * @throws VideoNotStoredException
     */
    public function getVideo(): Video
    {
        if (!$this->video instanceof Video) {
            throw $this->exception;
        }

        return $this->video;
    }

    function exceptionOccurred(VideoNotStoredException $exception): void
    {
        $this->exception = $exception;
    }
}