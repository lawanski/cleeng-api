<?php
declare(strict_types=1);

namespace Cleeng\Video\AssignVideoToCategory;

class AssignVideoToCategory
{
    private $assignVideoToCategoryQueryObject;

    function __construct(AssignVideoToCategoryQueryObject $assignVideoToCategoryQueryObject)
    {
        $this->assignVideoToCategoryQueryObject = $assignVideoToCategoryQueryObject;
    }

    function execute(AssignVideoToCategoryRequest $request, AssignVideoToCategoryPresenter $presenter)
    {
        $response = new AssignVideoToCategoryResponse();

        try {
            $this->assignVideoToCategoryQueryObject->execute(
                $request->getVideoId(),
                $request->getCategoryId()
            );
        } catch (VideoCouldNotBeAssignedToCategoryException $exception) {
            $response->exceptionOccurred($exception);
        }

        $presenter->present($response);
    }
}