<?php
declare(strict_types=1);

namespace Cleeng\Video\AssignVideoToCategory;

class AssignVideoToCategoryResponse
{
    private $exception;

    function exceptionOccurred(VideoCouldNotBeAssignedToCategoryException $exception): void
    {
        $this->exception = $exception;
    }

    /**
     * @throws VideoCouldNotBeAssignedToCategoryException
     */
    function isVideoSuccessfullyAssignedToCategory(): bool
    {
        if ($this->exception instanceof VideoCouldNotBeAssignedToCategoryException) {
            throw $this->exception;
        }

        return true;
    }
}