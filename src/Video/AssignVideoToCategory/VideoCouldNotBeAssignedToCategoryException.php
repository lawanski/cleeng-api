<?php
declare(strict_types=1);

namespace Cleeng\Video\AssignVideoToCategory;

use Cleeng\Video\VideoException;

class VideoCouldNotBeAssignedToCategoryException extends VideoException
{
    protected $message = 'Video could not be assigned to category';
    protected $code = 422;
}