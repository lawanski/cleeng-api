<?php
declare(strict_types=1);

namespace Cleeng\Video\AssignVideoToCategory;

interface AssignVideoToCategoryPresenter
{
    function present(AssignVideoToCategoryResponse $response): void;
    function view();
}