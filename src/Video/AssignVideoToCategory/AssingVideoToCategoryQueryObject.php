<?php
declare(strict_types=1);

namespace Cleeng\Video\AssignVideoToCategory;

interface AssignVideoToCategoryQueryObject
{
    /**
     * @throws VideoCouldNotBeAssignedToCategoryException
     */
    function execute(string $videoId, int $categoryId): void;
}