<?php
declare(strict_types=1);

namespace Cleeng\Video\AssignVideoToCategory;

class AssignVideoToCategoryRequest
{
    private $videoId;
    private $categoryId;

    function __construct($videoId, $categoryId)
    {
        $this->videoId = $videoId;
        $this->categoryId = $categoryId;
    }

    public function getVideoId(): string
    {
        return (string)$this->videoId;
    }

    public function getCategoryId(): int
    {
        return (int)$this->categoryId;
    }
}