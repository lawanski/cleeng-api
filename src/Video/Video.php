<?php
declare(strict_types=1);

namespace Cleeng\Video;

class Video
{
    private $id;
    private $name;
    private $link;
    private $event;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link)
    {
        $this->link = $link;
    }

    public function getEvent(): bool
    {
        return $this->event;
    }

    public function setEvent(bool $event): void
    {
        $this->event = $event;
    }
}