<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription')->insert([
            'id' => 1,
            'name' => 'silver',
            'duration' => 30
        ]);
        DB::table('subscription')->insert([
            'id' => 2,
            'name' => 'gold',
            'duration' => 30
        ]);
        DB::table('subscription')->insert([
            'id' => 3,
            'name' => 'season pass',
            'duration' => 7
        ]);
    }
}
