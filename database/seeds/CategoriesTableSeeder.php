<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            'id' => 1,
            'name' => 'action',
        ]);
        DB::table('category')->insert([
            'id' => 2,
            'name' => 'adventure',
        ]);
        DB::table('category')->insert([
            'id' => 3,
            'name' => 'comedy',
        ]);
        DB::table('category')->insert([
            'id' => 4,
            'name' => 'drama',
        ]);
        DB::table('category')->insert([
            'id' => 5,
            'name' => 'horror',
        ]);
        DB::table('category')->insert([
            'id' => 6,
            'name' => 'war',
        ]);
    }
}
