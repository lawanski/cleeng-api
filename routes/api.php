<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('video', 'VideoController@store');
    Route::post('video/{videoId}/category/{categoryId}', 'VideoController@assignVideoToCategory');
    Route::post('category/{categoryId}/subscription/{subscriptionId}', 'CategoryController@assignCategoryToSubscription');
    Route::post('payment/user/{userId}/video/{videoId}', 'PaymentController@buyVideo');
    Route::post('payment/user/{userId}/subscription/{subscriptionId}', 'PaymentController@buySubscription');
    Route::get('stream/video/{videoId}/user/{userId}', 'StreamController@watch');
});