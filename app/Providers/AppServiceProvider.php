<?php

namespace App\Providers;

use App\Infrastructure\Category\AssignCategoryToSubscription\SQLAssignCategoryToSubscriptionQueryObject;
use App\Infrastructure\Category\SQLCategoryRepository;
use App\Infrastructure\Payment\SQLPaymentRepository;
use App\Infrastructure\Subscription\BuySubscription\SQLAssignSubscriptionToUserQueryObject;
use App\Infrastructure\Subscription\SQLSubscriptionRepository;
use App\Infrastructure\User\SQLUserRepository;
use App\Infrastructure\Video\AssignVideoToCategory\SQLAssignVideoToCategoryQueryObject;
use App\Infrastructure\Video\BuyVideo\SQLAssignVideoToUserQueryObject;
use App\Infrastructure\Video\SQLVideoRepository;
use App\Infrastructure\Video\WatchVideo\SQLDidUserBoughtAccessToVideoQueryObject;
use Cleeng\Category\AssignCategoryToSubscription\AssignCategoryToSubscriptionQueryObject;
use Cleeng\Category\CategoryRepository;
use Cleeng\Payment\PaymentRepository;
use Cleeng\Subscription\BuySubscription\AssignSubscriptionToUserQueryObject;
use Cleeng\Subscription\SubscriptionRepository;
use Cleeng\User\UserRepository;
use Cleeng\Video\AssignVideoToCategory\AssignVideoToCategoryQueryObject;
use Cleeng\Video\BuyVideo\AssignVideoToUserQueryObject;
use Cleeng\Video\VideoRepository;
use Cleeng\Video\WatchVideo\DidUserBoughtAccessToVideoQueryObject;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(VideoRepository::class, SQLVideoRepository::class);
        $this->app->bind(CategoryRepository::class, SQLCategoryRepository::class);
        $this->app->bind(SubscriptionRepository::class, SQLSubscriptionRepository::class);
        $this->app->bind(UserRepository::class, SQLUserRepository::class);
        $this->app->bind(PaymentRepository::class, SQLPaymentRepository::class);
        $this->app->bind(AssignVideoToCategoryQueryObject::class, SQLAssignVideoToCategoryQueryObject::class);
        $this->app->bind(AssignCategoryToSubscriptionQueryObject::class, SQLAssignCategoryToSubscriptionQueryObject::class);
        $this->app->bind(AssignVideoToUserQueryObject::class, SQLAssignVideoToUserQueryObject::class);
        $this->app->bind(DidUserBoughtAccessToVideoQueryObject::class, SQLDidUserBoughtAccessToVideoQueryObject::class);
        $this->app->bind(AssignSubscriptionToUserQueryObject::class, SQLAssignSubscriptionToUserQueryObject::class);
    }
}
