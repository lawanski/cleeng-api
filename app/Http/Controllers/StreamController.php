<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Infrastructure\Video\WatchVideo\JSONWatchVideoPresenter;
use Cleeng\Video\WatchVideo\WatchVideo;
use Cleeng\Video\WatchVideo\WatchVideoRequest;

class StreamController extends Controller
{
    function watch($videoId, $userId, WatchVideo $watchVideo)
    {
        $request = new WatchVideoRequest($userId, $videoId);
        $presenter = new JSONWatchVideoPresenter();

        $watchVideo->execute($request, $presenter);

        return $presenter->view();
    }
}