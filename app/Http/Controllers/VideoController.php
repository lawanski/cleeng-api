<?php

namespace App\Http\Controllers;

use App\Infrastructure\Video\AssignVideoToCategory\JSONAssignVideoToCategoryPresenter;
use App\Infrastructure\Video\StoreVideo\JSONStoreVideoPresenter;
use Cleeng\Video\AssignVideoToCategory\AssignVideoToCategory;
use Cleeng\Video\AssignVideoToCategory\AssignVideoToCategoryRequest;
use Cleeng\Video\StoreVideo\StoreVideo;
use Cleeng\Video\StoreVideo\StoreVideoRequest;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function store(Request $request, StoreVideo $useCase)
    {
        $storeVideoRequest = new StoreVideoRequest(
            $request->get('name'),
            $request->get('link'),
            $request->get('event', false)
        );
        $presenter = new JSONStoreVideoPresenter();

        $useCase->execute($storeVideoRequest, $presenter);

        return $presenter->view();
    }

    public function assignVideoToCategory($videoId, $categoryId, AssignVideoToCategory $assignVideoToCategory)
    {
        $assignVideoToCategoryRequest = new AssignVideoToCategoryRequest($videoId, $categoryId);
        $presenter = new JSONAssignVideoToCategoryPresenter();

        $assignVideoToCategory->execute($assignVideoToCategoryRequest, $presenter);

        return $presenter->view();
    }
}
