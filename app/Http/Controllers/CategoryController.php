<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Infrastructure\Category\AssignCategoryToSubscription\JSONAssignCategoryToSubscriptionPresenter;
use Cleeng\Category\AssignCategoryToSubscription\AssignCategoryToSubscription;
use Cleeng\Category\AssignCategoryToSubscription\AssignCategoryToSubscriptionRequest;

class CategoryController extends Controller
{
    function assignCategoryToSubscription(
        $categoryId,
        $subscriptionId,
        AssignCategoryToSubscription $assignCategoryToSubscription
    ) {
        $request = new AssignCategoryToSubscriptionRequest($categoryId, $subscriptionId);
        $presenter = new JSONAssignCategoryToSubscriptionPresenter();

        $assignCategoryToSubscription->execute($request, $presenter);

        return $presenter->view();
    }
}