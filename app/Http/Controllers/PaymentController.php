<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Infrastructure\Subscription\BuySubscription\JSONBuySubscriptionPresenter;
use App\Infrastructure\Video\BuyVideo\JSONBuyVideoPresenter;
use Cleeng\Subscription\BuySubscription\BuySubscription;
use Cleeng\Subscription\BuySubscription\BuySubscriptionRequest;
use Cleeng\Video\BuyVideo\BuyVideo;
use Cleeng\Video\BuyVideo\BuyVideoRequest;

class PaymentController extends Controller
{
    function buyVideo($userId, $videoId, BuyVideo $buyVideo)
    {
        $request = new BuyVideoRequest($userId, $videoId);
        $presenter = new JSONBuyVideoPresenter();

        $buyVideo->execute($request, $presenter);

        return $presenter->view();
    }

    function buySubscription($userId, $subscriptionId, BuySubscription $buySubscription)
    {
        $request = new BuySubscriptionRequest($userId, $subscriptionId);
        $presenter = new JSONBuySubscriptionPresenter();

        $buySubscription->execute($request, $presenter);

        return $presenter->view();
    }
}