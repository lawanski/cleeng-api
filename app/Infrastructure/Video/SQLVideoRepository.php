<?php
declare(strict_types=1);

namespace App\Infrastructure\Video;

use Cleeng\Video\Video;
use Cleeng\Video\VideoNotFoundException;
use Cleeng\Video\VideoNotStoredException;
use Cleeng\Video\VideoRepository;
use Illuminate\Support\Facades\DB;

class SQLVideoRepository implements VideoRepository
{
    /**
     * @throws VideoNotStoredException
     */
    function insert(Video $video): void
    {
        $result = DB::table('video')->insert([
            'id' => $video->getId(),
            'name' => $video->getName(),
            'link' => $video->getLink(),
            'event' => $video->getEvent()
        ]);

        if (!$result) {
            throw new VideoNotStoredException();
        }
    }

    /**
     * @throws VideoNotFoundException
     */
    function find(string $videoId): Video
    {
        $rawVideo = DB::selectOne('select name, link, event from video where id = :id', ['id' => $videoId]);

        if (empty($rawVideo)) {
            throw new VideoNotFoundException();
        }

        $video = new Video();
        $video->setId($videoId);
        $video->setName($rawVideo->name);
        $video->setLink($rawVideo->link);
        $video->setEvent((bool)$rawVideo->event);

        return $video;
    }
}