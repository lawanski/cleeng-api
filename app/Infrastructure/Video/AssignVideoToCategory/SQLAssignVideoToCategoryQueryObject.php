<?php
declare(strict_types=1);

namespace App\Infrastructure\Video\AssignVideoToCategory;

use Cleeng\Category\Category;
use Cleeng\Category\CategoryNotFoundException;
use Cleeng\Category\CategoryRepository;
use Cleeng\Video\AssignVideoToCategory\AssignVideoToCategoryQueryObject;
use Cleeng\Video\AssignVideoToCategory\VideoCouldNotBeAssignedToCategoryException;
use Cleeng\Video\Video;
use Cleeng\Video\VideoNotFoundException;
use Cleeng\Video\VideoRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class SQLAssignVideoToCategoryQueryObject implements AssignVideoToCategoryQueryObject
{
    private $videoRepository;
    private $categoryRepository;

    public function __construct(
        VideoRepository $videoRepository,
        CategoryRepository $categoryRepository
    ) {
        $this->videoRepository = $videoRepository;
        $this->categoryRepository = $categoryRepository;
    }

    function execute(string $videoId, int $categoryId): void
    {
        try {
            $video = $this->videoRepository->find($videoId);
            $category = $this->categoryRepository->find($categoryId);

            $this->assignVideoToCategory($video, $category);
        } catch (VideoNotFoundException|CategoryNotFoundException $exception) {
            throw new VideoCouldNotBeAssignedToCategoryException($exception->getMessage(), $exception->getCode());
        } catch (VideoCouldNotBeAssignedToCategoryException $exception) {
            throw $exception;
        }
    }

    private function assignVideoToCategory(Video $video, Category $category): void
    {
        try {
            $result = DB::insert('insert into category_video (category_id, video_id) values (?, ?)', [
                $category->getId(),
                $video->getId()
            ]);

            if (!$result) {
                throw new VideoCouldNotBeAssignedToCategoryException();
            }
        } catch (\Throwable $exception) {
            throw new VideoCouldNotBeAssignedToCategoryException('Unknown error',
                JsonResponse::HTTP_CONFLICT);
        }
    }
}