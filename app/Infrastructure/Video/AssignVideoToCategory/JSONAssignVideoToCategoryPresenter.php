<?php
declare(strict_types=1);

namespace App\Infrastructure\Video\AssignVideoToCategory;

use Cleeng\Video\AssignVideoToCategory\AssignVideoToCategoryPresenter;
use Cleeng\Video\AssignVideoToCategory\AssignVideoToCategoryResponse;
use Cleeng\Video\AssignVideoToCategory\VideoCouldNotBeAssignedToCategoryException;
use Illuminate\Http\JsonResponse;

class JSONAssignVideoToCategoryPresenter implements AssignVideoToCategoryPresenter
{
    private $view;

    public function __construct()
    {
        $this->view = new JsonResponse([], JsonResponse::HTTP_CREATED);
    }

    function present(AssignVideoToCategoryResponse $response): void
    {
        try {
            $response->isVideoSuccessfullyAssignedToCategory();
        } catch (VideoCouldNotBeAssignedToCategoryException $exception) {
            $this->view->setStatusCode($exception->getCode());
            $this->view->setData([
                'message' => $exception->getMessage()
            ]);
        }
    }

    function view(): JsonResponse
    {
        return $this->view;
    }
}