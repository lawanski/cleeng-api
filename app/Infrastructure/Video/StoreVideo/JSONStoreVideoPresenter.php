<?php
declare(strict_types=1);

namespace App\Infrastructure\Video\StoreVideo;

use Cleeng\Video\StoreVideo\StoreVideoPresenter;
use Cleeng\Video\StoreVideo\StoreVideoResponse;
use Cleeng\Video\VideoNotStoredException;
use Illuminate\Http\JsonResponse;

class JSONStoreVideoPresenter implements StoreVideoPresenter
{
    private $view;

    function __construct()
    {
        $this->view = new JsonResponse([],JsonResponse::HTTP_CREATED);
    }

    function present(StoreVideoResponse $response): void
    {
        try {
            $video = $response->getVideo();
            $this->view->setData([
                'videoId' => $video->getId()
            ]);
        } catch (VideoNotStoredException $exception) {
            $this->view->setStatusCode($exception->getCode());
            $this->view->setData([
                'message' => $exception->getMessage()
            ]);
        }
    }

    function view(): JsonResponse
    {
        return $this->view;
    }
}