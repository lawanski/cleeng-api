<?php
declare(strict_types=1);

namespace App\Infrastructure\Video\BuyVideo;

use Cleeng\Video\BuyVideo\BuyVideoPresenter;
use Cleeng\Video\BuyVideo\BuyVideoResponse;
use Cleeng\Video\BuyVideo\UserDidNotBoughtVideoException;
use Illuminate\Http\JsonResponse;

class JSONBuyVideoPresenter implements BuyVideoPresenter
{
    private $view;

    public function __construct()
    {
        $this->view = new JsonResponse([], JsonResponse::HTTP_CREATED);
    }

    function present(BuyVideoResponse $response): void
    {
        try {
            $response->isVideoBoughtSuccessfully();
        } catch (UserDidNotBoughtVideoException $exception) {
            $this->view->setStatusCode($exception->getCode());
            $this->view->setData([
                'message' => $exception->getMessage()
            ]);
        }
    }

    function view(): JsonResponse
    {
        return $this->view;
    }
}