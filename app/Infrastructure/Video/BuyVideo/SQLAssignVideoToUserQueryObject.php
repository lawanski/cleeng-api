<?php
declare(strict_types=1);

namespace App\Infrastructure\Video\BuyVideo;

use Cleeng\Payment\Payment;
use Cleeng\User\User;
use Cleeng\Video\BuyVideo\AssignVideoToUserQueryObject;
use Cleeng\Video\BuyVideo\VideoCouldNotBeAssignedToUserException;
use Cleeng\Video\Video;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class SQLAssignVideoToUserQueryObject implements AssignVideoToUserQueryObject
{
    /**
     * @throws VideoCouldNotBeAssignedToUserException
     */
    function execute(User $user, Video $video, Payment $payment): void
    {
        try {
            $result = DB::insert('insert into user_video (user_id, video_id, payment_id, created_at) values (?,?,?,?)', [
                $user->getId(),
                $video->getId(),
                $payment->getId(),
                (new \DateTime())->format('Y-m-d H:i:s')
            ]);

            if (!$result) {
                throw new VideoCouldNotBeAssignedToUserException();
            }
        } catch (\Throwable $exception) {
            throw new VideoCouldNotBeAssignedToUserException('Unknown error', JsonResponse::HTTP_CONFLICT);
        }
    }
}