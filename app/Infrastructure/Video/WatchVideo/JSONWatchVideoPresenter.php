<?php
declare(strict_types=1);

namespace App\Infrastructure\Video\WatchVideo;

use Cleeng\Video\WatchVideo\UserDontHaveAccessToVideoException;
use Cleeng\Video\WatchVideo\WatchVideoPresenter;
use Cleeng\Video\WatchVideo\WatchVideoResponse;
use Illuminate\Http\JsonResponse;

class JSONWatchVideoPresenter implements WatchVideoPresenter
{
    private $view;

    function __construct()
    {
        $this->view = new JsonResponse();
    }

    function present(WatchVideoResponse $response): void
    {
        try {
            $video = $response->getVideo();
            $this->view->setData([
                'name' => $video->getName(),
                'link' => $video->getLink()
            ]);
        } catch (UserDontHaveAccessToVideoException $exception) {
            $this->view->setStatusCode($exception->getCode());
            $this->view->setData([
                'message' => $exception->getMessage()
            ]);
        }
    }

    function view(): JsonResponse
    {
        return $this->view;
    }
}