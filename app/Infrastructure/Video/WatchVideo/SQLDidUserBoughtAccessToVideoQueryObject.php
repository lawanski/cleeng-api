<?php
declare(strict_types=1);

namespace App\Infrastructure\Video\WatchVideo;

use Cleeng\Video\Video;
use Cleeng\Video\VideoNotFoundException;
use Cleeng\Video\VideoRepository;
use Cleeng\Video\WatchVideo\DidUserBoughtAccessToVideoQueryObject;
use Cleeng\Video\WatchVideo\UserDontHaveAccessToVideoException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class SQLDidUserBoughtAccessToVideoQueryObject implements DidUserBoughtAccessToVideoQueryObject
{
    private $videoRepository;

    function __construct(VideoRepository $videoRepository)
    {
        $this->videoRepository = $videoRepository;
    }

    /**
     * @throws UserDontHaveAccessToVideoException
     */
    function execute(int $userId, string $videoId): Video
    {
        try {
            $result = DB::selectOne('select video_id from user_video where user_id=:userId and video_id=:videoId', [
                'userId' => $userId,
                'videoId' => $videoId
            ]);

            if (is_null($result)) {
                throw new UserDontHaveAccessToVideoException();
            }

            return $this->videoRepository->find($videoId);
        } catch (VideoNotFoundException $exception) {
            throw new UserDontHaveAccessToVideoException($exception->getMessage(), $exception->getCode());
        } catch (UserDontHaveAccessToVideoException $exception) {
            throw $exception;
        } catch (\Throwable $exception) {
            throw new UserDontHaveAccessToVideoException('Unknown error occurred', JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}