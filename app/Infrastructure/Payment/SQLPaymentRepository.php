<?php
declare(strict_types=1);

namespace App\Infrastructure\Payment;

use Cleeng\Payment\Payment;
use Cleeng\Payment\PaymentNotInsertedException;
use Cleeng\Payment\PaymentRepository;
use Illuminate\Support\Facades\DB;

class SQLPaymentRepository implements PaymentRepository
{
    /**
     * @throws PaymentNotInsertedException
     */
    function insert(Payment $payment): void
    {
        $result = DB::insert('insert into payment (user_id, created_at) VALUES (?,?)', [
            $payment->getUserId(),
            $payment->getCreatedAt()->format('Y-m-d H:i:s')
        ]);

        if (!$result) {
            throw new PaymentNotInsertedException();
        }

        $lastInsertedId = (int)DB::getPdo()->lastInsertId();

        $payment->setId($lastInsertedId);
    }
}