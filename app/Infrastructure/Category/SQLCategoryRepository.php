<?php
declare(strict_types=1);

namespace App\Infrastructure\Category;

use Cleeng\Category\Category;
use Cleeng\Category\CategoryNotFoundException;
use Cleeng\Category\CategoryRepository;
use Illuminate\Support\Facades\DB;

class SQLCategoryRepository implements CategoryRepository
{
    /**
     * @throws CategoryNotFoundException
     */
    function find(int $categoryId): Category
    {
        $rawCategory = DB::selectOne('select name from category where id = :id', ['id' => $categoryId]);

        if (empty($rawCategory)) {
            throw new CategoryNotFoundException();
        }

        $category = new Category();

        $category->setId($categoryId);
        $category->setName($rawCategory->name);

        return $category;
    }
}