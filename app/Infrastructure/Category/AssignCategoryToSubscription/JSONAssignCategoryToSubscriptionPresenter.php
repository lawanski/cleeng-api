<?php
declare(strict_types=1);

namespace App\Infrastructure\Category\AssignCategoryToSubscription;

use Cleeng\Category\AssignCategoryToSubscription\AssignCategoryToSubscriptionPresenter;
use Cleeng\Category\AssignCategoryToSubscription\AssignCategoryToSubscriptionResponse;
use Cleeng\Category\AssignCategoryToSubscription\CategoryCouldNotBeAssignedToSubscriptionException;
use Illuminate\Http\JsonResponse;

class JSONAssignCategoryToSubscriptionPresenter implements AssignCategoryToSubscriptionPresenter
{
    private $view;

    function __construct()
    {
        $this->view = new JsonResponse([], JsonResponse::HTTP_CREATED);
    }

    function present(AssignCategoryToSubscriptionResponse $response): void
    {
        try {
            $response->isCategorySuccessfullyAssignedToSubscription();
        } catch (CategoryCouldNotBeAssignedToSubscriptionException $exception) {
            $this->view->setStatusCode($exception->getCode());
            $this->view->setData([
                'message' => $exception->getMessage()
            ]);
        }
    }

    function view(): JsonResponse
    {
        return $this->view;
    }
}