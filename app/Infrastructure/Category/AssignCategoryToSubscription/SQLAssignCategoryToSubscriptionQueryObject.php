<?php
declare(strict_types=1);

namespace App\Infrastructure\Category\AssignCategoryToSubscription;

use Cleeng\Category\AssignCategoryToSubscription\AssignCategoryToSubscriptionQueryObject;
use Cleeng\Category\AssignCategoryToSubscription\CategoryCouldNotBeAssignedToSubscriptionException;
use Cleeng\Category\Category;
use Cleeng\Category\CategoryNotFoundException;
use Cleeng\Category\CategoryRepository;
use Cleeng\Subscription\Subscription;
use Cleeng\Subscription\SubscriptionNotFoundException;
use Cleeng\Subscription\SubscriptionRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class SQLAssignCategoryToSubscriptionQueryObject implements AssignCategoryToSubscriptionQueryObject
{
    private $categoryRepository;
    private $subscriptionRepository;

    function __construct(
        CategoryRepository $categoryRepository,
        SubscriptionRepository $subscriptionRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    /**
     * @throws CategoryCouldNotBeAssignedToSubscriptionException
     */
    function execute(int $categoryId, int $subscriptionId): void
    {
        try {
            $category = $this->categoryRepository->find($categoryId);
            $subscription = $this->subscriptionRepository->find($subscriptionId);

            $this->assignCategoryToSubscription($category, $subscription);
        } catch (CategoryNotFoundException|SubscriptionNotFoundException $exception) {
            throw new CategoryCouldNotBeAssignedToSubscriptionException($exception->getMessage(), $exception->getCode());
        } catch (CategoryCouldNotBeAssignedToSubscriptionException $exception) {
            throw $exception;
        }
    }

    /**
     * @throws CategoryCouldNotBeAssignedToSubscriptionException
     */
    private function assignCategoryToSubscription(
        Category $category,
        Subscription $subscription
    ) {
        try {
            $result = DB::insert('insert into category_subscription (category_id, subscription_id) values (?,?)', [
                $category->getId(),
                $subscription->getId()
            ]);

            if (!$result) {
                throw new CategoryCouldNotBeAssignedToSubscriptionException();
            }
        } catch (\Throwable $exception) {
            throw new CategoryCouldNotBeAssignedToSubscriptionException('Unknown error', JsonResponse::HTTP_CONFLICT);
        }
    }
}