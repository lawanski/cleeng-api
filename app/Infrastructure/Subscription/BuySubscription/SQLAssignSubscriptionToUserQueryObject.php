<?php
declare(strict_types=1);

namespace App\Infrastructure\Subscription\BuySubscription;

use Cleeng\Payment\Payment;
use Cleeng\Subscription\BuySubscription\AssignSubscriptionToUserQueryObject;
use Cleeng\Subscription\BuySubscription\SubscriptionCouldNotBeAssignedToUserException;
use Cleeng\Subscription\Subscription;
use Cleeng\User\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class SQLAssignSubscriptionToUserQueryObject implements AssignSubscriptionToUserQueryObject
{
    /**
     * @throws SubscriptionCouldNotBeAssignedToUserException
     */
    function execute(Subscription $subscription, User $user, Payment $payment): void
    {
        $createdAt = new \DateTime();
        $expiresAt = clone $createdAt;
        $expiresAt->add(new \DateInterval("P{$subscription->getDuration()}D"));

        try {
            $result = DB::insert('insert into user_subscription (user_id, subscription_id, payment_id, created_at, expires_at) values (?,?,?,?,?)', [
                $user->getId(),
                $subscription->getId(),
                $payment->getId(),
                $createdAt->format('Y-m-d H:i:s'),
                $expiresAt->format('Y-m-d H:i:s'),
            ]);

            if (!$result) {
                throw new SubscriptionCouldNotBeAssignedToUserException();
            }
        } catch (\Throwable $exception) {
            throw new SubscriptionCouldNotBeAssignedToUserException('Unknown error', JsonResponse::HTTP_CONFLICT);
        }
    }
}