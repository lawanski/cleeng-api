<?php
declare(strict_types=1);

namespace App\Infrastructure\Subscription\BuySubscription;

use Cleeng\Subscription\BuySubscription\BuySubscriptionPresenter;
use Cleeng\Subscription\BuySubscription\BuySubscriptionResponse;
use Cleeng\Subscription\BuySubscription\UserDidNotBoughtSubscriptionException;
use Illuminate\Http\JsonResponse;

class JSONBuySubscriptionPresenter implements BuySubscriptionPresenter
{
    private $view;

    public function __construct()
    {
        $this->view = new JsonResponse([], JsonResponse::HTTP_CREATED);
    }

    function present(BuySubscriptionResponse $response): void
    {
        try {
            $response->didUserBoughtSubscription();
        } catch (UserDidNotBoughtSubscriptionException $exception) {
            $this->view->setStatusCode($exception->getCode());
            $this->view->setData([
                'message' => $exception->getMessage()
            ]);
        }
    }

    function view(): JsonResponse
    {
        return $this->view;
    }
}