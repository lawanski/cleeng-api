<?php
declare(strict_types=1);

namespace App\Infrastructure\Subscription;

use Cleeng\Subscription\Subscription;
use Cleeng\Subscription\SubscriptionNotFoundException;
use Cleeng\Subscription\SubscriptionRepository;
use Illuminate\Support\Facades\DB;

class SQLSubscriptionRepository implements SubscriptionRepository
{
    /**
     * @throws SubscriptionNotFoundException
     */
    function find(int $subscriptionId): Subscription
    {
        $rawSubscription = DB::selectOne('select name, duration from subscription where id = :id', ['id' => $subscriptionId]);

        if (empty($rawSubscription)) {
            throw new SubscriptionNotFoundException();
        }

        $subscription = new Subscription();

        $subscription->setId($subscriptionId);
        $subscription->setName($rawSubscription->name);
        $subscription->setDuration($rawSubscription->duration);

        return $subscription;
    }
}