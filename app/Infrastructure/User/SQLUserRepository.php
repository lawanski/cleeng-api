<?php
declare(strict_types=1);

namespace App\Infrastructure\User;

use Cleeng\User\User;
use Cleeng\User\UserNotFoundException;
use Cleeng\User\UserRepository;
use Illuminate\Support\Facades\DB;

class SQLUserRepository implements UserRepository
{
    /**
     * @throws UserNotFoundException
     */
    function find(int $userId): User
    {
        $rawUser = DB::selectOne('select name from user where id = :id', ['id' => $userId]);

        if (empty($rawUser)) {
            throw new UserNotFoundException();
        }

        $user = new User();
        $user->setId($userId);
        $user->setName($rawUser->name);

        return $user;
    }
}